package pe.com.crud.entidades;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "cliente", catalog = "desadb")
public class Cliente {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idCliente")
	private int idCliente;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apePaterno")
	private String apePaterno;

	@Column(name = "apeMaterno")
	private String apeMaterno;

	@Column(name = "nroDocumento")
	private String nroDocumento;

	@Column(name = "direccion")
	private String direccion;

	@Column(name = "correo")
	private String correo;

	@Column(name = "telefono")
	private String telefono;

	@Column(name = "edad")
	private int edad;

	@Column(name = "estadoCivil")
	private String estadoCivil;

	@Column(name = "nroHijos")
	private int ntoHijos;

	@Column(name = "fechaNacimiento")
	private Date fechaNacimiento;

	@Column(name = "estado")
	private String estado;

	public Cliente() {
	}

	public Cliente(int idCliente, String nombre, String apePaterno, String apeMaterno, String nroDocumento,
			String direccion, String correo, String telefono, int edad, String estadoCivil, int ntoHijos,
			Date fechaNacimiento, String estado) {
		super();
		this.idCliente = idCliente;
		this.nombre = nombre;
		this.apePaterno = apePaterno;
		this.apeMaterno = apeMaterno;
		this.nroDocumento = nroDocumento;
		this.direccion = direccion;
		this.correo = correo;
		this.telefono = telefono;
		this.edad = edad;
		this.estadoCivil = estadoCivil;
		this.ntoHijos = ntoHijos;
		this.fechaNacimiento = fechaNacimiento;
		this.estado = estado;
	}

	public int getIdCliente() {
		return idCliente;
	}

	public void setIdCliente(int idCliente) {
		this.idCliente = idCliente;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApePaterno() {
		return apePaterno;
	}

	public void setApePaterno(String apePaterno) {
		this.apePaterno = apePaterno;
	}

	public String getApeMaterno() {
		return apeMaterno;
	}

	public void setApeMaterno(String apeMaterno) {
		this.apeMaterno = apeMaterno;
	}

	public String getNroDocumento() {
		return nroDocumento;
	}

	public void setNroDocumento(String nroDocumento) {
		this.nroDocumento = nroDocumento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getEstadoCivil() {
		return estadoCivil;
	}

	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}

	public int getNtoHijos() {
		return ntoHijos;
	}

	public void setNtoHijos(int ntoHijos) {
		this.ntoHijos = ntoHijos;
	}

	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

}
