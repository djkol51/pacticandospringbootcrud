package pe.com.crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import pe.com.crud.request.ConsultaClienteRequest;
import pe.com.crud.request.RegistraClienteRequest;
import pe.com.crud.response.ConsultaClienteResponse;
import pe.com.crud.response.GenericoResponse;
import pe.com.crud.response.RegistraClienteResponse;
import pe.com.crud.service.ClienteService;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;

	@PostMapping(value = "listar", headers = "Accept=application/json")
	public ResponseEntity<ConsultaClienteResponse> listaCliente(@RequestBody ConsultaClienteRequest req) {

		ConsultaClienteResponse response = new ConsultaClienteResponse();

		ResponseEntity<ConsultaClienteResponse> responseEntity = null;

		try {
			
			response = clienteService.listaCliente(req);
			
			if(response.getCodigoRespuesta().equals("0")) {
				responseEntity = new ResponseEntity<>(response,HttpStatus.OK);
			}else {
				responseEntity = new ResponseEntity<>(response,HttpStatus.NOT_FOUND);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return responseEntity;

	}
	
	
	@PostMapping(value = "registrar", headers ="Accept=application/json")
	public ResponseEntity<RegistraClienteResponse> registrarCliente(@RequestBody RegistraClienteRequest req){
		
		
		ResponseEntity<RegistraClienteResponse> responseEntity = null;
		
		RegistraClienteResponse response = new RegistraClienteResponse();
		
		try {
			
			response = clienteService.registraCliente(req);
			
			if(response.getCodigoRespuesta().equals("0")) {
				responseEntity = new ResponseEntity<RegistraClienteResponse>(response,HttpStatus.OK);
			}else {
				responseEntity = new ResponseEntity<RegistraClienteResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return responseEntity;
		
	}
	
	@PutMapping(value = "actualizar", headers ="Accept=application/json")
	public ResponseEntity<GenericoResponse> actualizarCliente(@RequestBody RegistraClienteRequest req){
		
		GenericoResponse response = new GenericoResponse();
		
		ResponseEntity<GenericoResponse> responseEntity = null;
		
		try {
			
			response = clienteService.actualizarCliente(req);
			
			if(response.getCodigoRespuesta().equals("0")) {
				responseEntity = new ResponseEntity<GenericoResponse>(response,HttpStatus.OK);
			}else {
				responseEntity = new ResponseEntity<GenericoResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return responseEntity;
		
	}
	
	@DeleteMapping(value ="eliminar",headers ="Accept=application/json")
	public ResponseEntity<GenericoResponse> eliminarCliente(
			@RequestParam int idCliente){
		
		ResponseEntity<GenericoResponse> responseEntity = null;
		
		GenericoResponse response = new GenericoResponse();
		
		try {
			
			response = clienteService.eliminarCliente(idCliente);
			
			if(response.getCodigoRespuesta().equals("0")) {
				responseEntity = new ResponseEntity<GenericoResponse>(response,HttpStatus.OK);
			}else {
				responseEntity = new ResponseEntity<GenericoResponse>(response,HttpStatus.INTERNAL_SERVER_ERROR);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return responseEntity;
		
	}

}
