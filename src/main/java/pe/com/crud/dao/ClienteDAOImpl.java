package pe.com.crud.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import pe.com.crud.entidades.Cliente;

@Repository(value = "clienteDAO")
@Transactional
public class ClienteDAOImpl implements ClienteDAO {

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<Cliente> listaCliente(String nroDoc) {

		String sql = "";

		List<Cliente> lista = new ArrayList<>();

		try {

			if (!nroDoc.equals("") || !nroDoc.isEmpty()) {
				sql = sql + "from Cliente c where c.nroDocumento ='" + nroDoc + "'";
			} else {
				sql = sql + "from Cliente c";
			}

			Query q = em.createQuery(sql);

			lista = q.getResultList();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return lista;
	}

	@Override
	public Map<String, String> registrarCliente(Cliente cli) {

		Map<String, String> result = new HashMap<>();

		try {

			List<Cliente> buscaReg = listaCliente(cli.getNroDocumento());

			if (buscaReg.isEmpty()) {

				em.persist(cli);
				result.put("codigoRespuesta", "0");
				result.put("mensajeRespuesta", "Operacion Exitosa");
				result.put("codigoGenerado", String.valueOf(cli.getIdCliente()));

			} else {
				result.put("codigoRespuesta", "1");
				result.put("mensajeRespuesta", "Registro existente");
				result.put("codigoGenerado", "0");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;

	}

	@Override
	public Map<String, String> actualizarCliente(Cliente cli) {

		Map<String, String> result = new HashMap<>();

		try {

			Cliente buscaReg = em.find(Cliente.class, cli.getIdCliente());

			if (buscaReg.getIdCliente()!=0) {

				em.merge(cli);
				result.put("codigoRespuesta", "0");
				result.put("mensajeRespuesta", "Operacion Exitosa");
				

			} else {
				result.put("codigoRespuesta", "1");
				result.put("mensajeRespuesta", "No se pudo actualizar");
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	@Override
	public Map<String, String> eliminarCliente(int idCliente) {
		Map<String, String> result = new HashMap<>();

		try {

			Cliente buscaReg = em.find(Cliente.class, idCliente);

			if (buscaReg.getIdCliente()!=0) {

				em.remove(buscaReg);
				result.put("codigoRespuesta", "0");
				result.put("mensajeRespuesta", "Operacion Exitosa");
				

			} else {
				result.put("codigoRespuesta", "1");
				result.put("mensajeRespuesta", "Registro no encontrado para eliminar");

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

}
