package pe.com.crud.dao;

import java.util.List;
import java.util.Map;

import pe.com.crud.entidades.Cliente;

public interface ClienteDAO {
	public List<Cliente> listaCliente(String nroDoc); 
	public Map<String, String> registrarCliente(Cliente cli);
	public Map<String, String> actualizarCliente(Cliente cli);
	public Map<String, String> eliminarCliente(int idCliente);
}
