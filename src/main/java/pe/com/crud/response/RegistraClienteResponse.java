package pe.com.crud.response;

public class RegistraClienteResponse extends GenericoResponse {

	private int codigoGenerado;

	public int getCodigoGenerado() {
		return codigoGenerado;
	}

	public void setCodigoGenerado(int codigoGenerado) {
		this.codigoGenerado = codigoGenerado;
	}
	
	
}
