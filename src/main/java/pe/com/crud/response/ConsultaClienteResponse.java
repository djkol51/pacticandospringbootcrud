package pe.com.crud.response;

import java.util.List;

import pe.com.crud.entidades.Cliente;

public class ConsultaClienteResponse extends GenericoResponse {

	private List<Cliente> listaCliente;

	public List<Cliente> getListaCliente() {
		return listaCliente;
	}

	public void setListaCliente(List<Cliente> listaCliente) {
		this.listaCliente = listaCliente;
	}
	
	
	
}
