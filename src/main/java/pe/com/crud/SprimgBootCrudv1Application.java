package pe.com.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SprimgBootCrudv1Application {

	public static void main(String[] args) {
		SpringApplication.run(SprimgBootCrudv1Application.class, args);
	}

}

