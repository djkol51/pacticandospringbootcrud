package pe.com.crud.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pe.com.crud.dao.ClienteDAO;
import pe.com.crud.entidades.Cliente;
import pe.com.crud.request.ConsultaClienteRequest;
import pe.com.crud.request.RegistraClienteRequest;
import pe.com.crud.response.ConsultaClienteResponse;
import pe.com.crud.response.GenericoResponse;
import pe.com.crud.response.RegistraClienteResponse;

@Service(value = "clienteService")
public class ClienteServiceImpl implements ClienteService {

	@Autowired
	private ClienteDAO clienteDAO;

	@Override
	public ConsultaClienteResponse listaCliente(ConsultaClienteRequest req) {

		ConsultaClienteResponse response = new ConsultaClienteResponse();

		List<Cliente> lista = new ArrayList<>();
		try {

			String nroDoc = req.getNroDoc();

			lista = clienteDAO.listaCliente(nroDoc);

			if (!lista.isEmpty()) {

				response.setCodigoRespuesta("0");
				response.setMensajeRespuesta("Operacion Exitosa");

			} else {

				response.setCodigoRespuesta("1");
				response.setMensajeRespuesta("No hay registros");

			}

			response.setListaCliente(lista);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public RegistraClienteResponse registraCliente(RegistraClienteRequest req) {

		RegistraClienteResponse response = new RegistraClienteResponse();

		Map<String, String> result = new HashMap<>();

		try {

			Cliente cli = req.getCliente();

			result = clienteDAO.registrarCliente(cli);

			response.setCodigoRespuesta(result.get("codigoRespuesta").toString());
			response.setMensajeRespuesta(result.get("mensajeRespuesta").toString());
			response.setCodigoGenerado(Integer.valueOf(result.get("codigoGenerado").toString()));

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public GenericoResponse actualizarCliente(RegistraClienteRequest req) {

		GenericoResponse response = new GenericoResponse();

		Map<String, String> result = new HashMap<>();

		try {

			Cliente cli = req.getCliente();

			result = clienteDAO.actualizarCliente(cli);

			response.setCodigoRespuesta(result.get("codigoRespuesta").toString());
			response.setMensajeRespuesta(result.get("mensajeRespuesta").toString());
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;

	}

	@Override
	public GenericoResponse eliminarCliente(int idCliente) {

		GenericoResponse response = new GenericoResponse();

		Map<String, String> result = new HashMap<>();

		try {
			
			result = clienteDAO.eliminarCliente(idCliente);

			response.setCodigoRespuesta(result.get("codigoRespuesta").toString());
			response.setMensajeRespuesta(result.get("mensajeRespuesta").toString());
			

		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
		
	}

}
