package pe.com.crud.service;

import pe.com.crud.request.ConsultaClienteRequest;
import pe.com.crud.request.RegistraClienteRequest;
import pe.com.crud.response.ConsultaClienteResponse;
import pe.com.crud.response.GenericoResponse;
import pe.com.crud.response.RegistraClienteResponse;

public interface ClienteService {
	public ConsultaClienteResponse listaCliente(ConsultaClienteRequest req);
	public RegistraClienteResponse registraCliente(RegistraClienteRequest req);
	public GenericoResponse actualizarCliente(RegistraClienteRequest req);
	public GenericoResponse eliminarCliente(int idCliente);
}
