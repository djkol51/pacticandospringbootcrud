package pe.com.crud.request;

import pe.com.crud.entidades.Cliente;

public class RegistraClienteRequest {
	
	private Cliente cliente;

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	
	
}
